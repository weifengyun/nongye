package com.nongye.mapper;


import com.nongye.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//@Mapper         //声明是一个Mapper,与springbootApplication中的@MapperScan二选一写上即可
@Repository
public interface Usermapper {
    List<User> loginUser(@Param(value = "userName") String userName,@Param(value = "password")  String password);
}
