package com.nongye.controller;

import com.nongye.comment.Constants;
import com.nongye.comment.JsonResult;
import com.nongye.entity.User;
import com.nongye.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class UserController {
    @Autowired
private UserService userService;
    @RequestMapping(value = "/loginUser", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
private List<User> loginUser(@RequestParam("userName") String userName, @RequestParam("password") String password) {

        /*JsonResult result=null;*/
        List<User> users = userService.loginUser(userName,password);
        /*try{
        if (users.size()!=0){
            result=new JsonResult(Constants.STATUS_SUCCESS, "用户名与密码不付！");
        }
        }catch (Exception e){
            result=new JsonResult(Constants.STATUS_FAILURE, "登录异常", e.getMessage());
        }*/
return  users;
    }
}

