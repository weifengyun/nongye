package com.nongye.service.serviceImp;

import com.nongye.entity.User;
import com.nongye.mapper.Usermapper;
import com.nongye.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service(value = "userService")
public class UserServiceImp implements UserService {
    @Resource
private Usermapper userMapper;
    @Override
public List<User> loginUser(String userName,String password) {

        return userMapper.loginUser(userName,password);
    }
}
