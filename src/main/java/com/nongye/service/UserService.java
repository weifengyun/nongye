package com.nongye.service;


import com.nongye.entity.User;

import java.util.List;

public interface UserService {

        List<User> loginUser(String userName,String password);

    }

